# to-do-app-vue

Este proyecto fue realizado con Vue 3 y Vite, para la clase de Programación Web II.

## Preparacion del proyecto

```sh
npm install
```

### Iniciacion del servidor

```sh
npm run dev
```

### Compilación y minificación para producción

```sh
npm run build
```
